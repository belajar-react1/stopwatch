import { useEffect, useState } from "react";
import Button from "./Button";

function App() {

  const [time, setTime] = useState(3590);

  const [isRunning, setIsRunning] = useState(false);

  useEffect(() => {
    let intervalId;
    if (isRunning) {
      intervalId = setInterval(() => setTime(time + 1), 1000);
    }
    return () => clearInterval(intervalId);
  }, [isRunning, time]);

  const hours = Math.floor(time / 3600);

  const minutes = Math.floor((time / 60) % 60);

  const seconds = Math.floor((time % 60));

  const start = () => {
    setIsRunning(true);
  }

  const stop = () => {
    setIsRunning(false);
  }

  const reset = () => {
    setIsRunning(false);
    setTime(0);
  }

  return (
    <div className="grid place-items-center p-5 gap-4">
      <div className="bg-blue-400 text-center flex justify-center items-center w-[75%] h-[500px] text-[8rem]">
        {hours.toString().padStart(2, '0')}:{minutes.toString().padStart(2, '0')}:{seconds.toString().padStart(2, '0')}
      </div>
      <div className="w-full flex justify-center items-center border h-56 border-black gap-10">
        <Button bg={'yellow'} text={"Reset"} onClick={reset} />
        <Button bg={'orange'} text={"Start"} onClick={start} />
        <Button bg={'green'} text={"Stop"} onClick={stop} />
      </div>
    </div>
  );
}

export default App;
