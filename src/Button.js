import React from 'react'

const Button = ({ bg, text, onClick }) => {
    return (
        <button
            style={{
                backgroundColor: bg
            }}
            onClick={onClick}
            className={`min-h-[100px] min-w-[300px] text-2xl font-bold rounded-full focus:bg-green-500`}>
            {text}
        </button>
    )
}

export default Button